//: Playground - noun: a place where people can play

import UIKit

protocol Tagable
{
    var name: String {get set}
    func tagged<T: Tagable>(_ item: T)
}

extension Tagable
{
    
    func tagged<T: Tagable>(_ item: T)
    {
        print("Tagged -- \(item.name) is it!")
    }
}

struct Person: Tagable
{
    var name: String
}

struct Animal: Tagable
{
    var name: String
}


let jack = Person(name: "Jack")
let duke = Animal(name: "Duke")


jack.tagged(duke)
duke.tagged(jack)