//: Playground - noun: a place where people can play

import UIKit

protocol Callable
{
    var phone: String { get set }
    func call(_ callee: Callable)
}

extension Callable
{
    func call(_ callee: Callable)
    {
        debugPrint("Calling \(callee.phone)")
    }
    
    func call<T: Callable>(callee: T)
    {
        debugPrint("Calling \(callee.phone)")
    }
}

struct Professor: Callable
{
    var facultyID: String
    var name: String
    var phone: String
    var department: String
}

struct Student: Callable
{
    var studentID: String
    var name: String
    var phone: String
    var major: String
}

let steve = Professor(facultyID: "123", name: "Steve", phone: "123-456-7890", department: "English")
let tim = Student(studentID: "456", name: "Tim", phone: "987-654-3210", major: "Math")

steve.call(tim)
tim.call(steve)


