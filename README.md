
# Description

This repo contains several Objective-C & Swift projects that demonstrates and shows understanding of the following coding concepts:

## Delegation Design Pattern

The verb, “to delegate”, meaning “to give control”

<http://www.andrewcbancroft.com/2015/04/08/how-delegation-works-a-swift-developer-guide/>

For delegation to occur in software, you’d have a situation where one class (a delegator class) would give control or responsibility for some behavioral logic to another class called a delegate.

So how does one class delegate behavioral logic to another class? With iOS and Swift, the delegation design pattern is achieved by utilizing an abstraction layer called a protocol.

A protocol defines a blueprint of methods, properties, and other requirements that suit a particular task or piece of functionality.

Protocols are an “abstraction”, because they do not provide implementation details in their declaration… Only function and property names. Like an outline, or as Apple puts it, a blueprint.

### Protocols are similar to contracts.

A contract is the “thing” in the middle of two parties who are trying to negotiate a deal. To one party, the contract is a guarantee of some terms that will be satisfied. To the other party, the contract is a set of obligations.

In the delegation design pattern, protocols serve the same kind middle-man role as a contract. To the delegator class, the protocol is a guarantee that some behavior will be supplied by the other party (the delegate). To the delegate class, the protocol is a set of obligations – things it must implement when it “signs the contract”, or in Swift terms, “adopts the protocol”.

### Listing the players

Stepping back from this description, we see three players involved:

* A protocol defining the responsibilities that will be delegated
* A delegator, which depends on an instance of something conforming to that protocol
* A delegate, which adopts the protocol and implements its requirements

![protocol diagram](http://www.andrewcbancroft.com/wp-content/uploads/2015/02/Delegate_Environment.png)

### Setting up the delegator

A delegator class typically defines a variable property with the word “delegate” somewhere in the name (oftentimes the property is simply named delegate if that’s explanatory enough).

### Setting up the delegate

The delegate class is what adopts the protocol and implements its requirements.

In the class declaration, the name of the protocol(s) that the class intends to adopt are listed separated by commas after the name of the superclass (if the class inherits from a superclass):

```
class MyClass: SuperClass, Protocol1, Protocol2 { ... }
```
When the delegator class gets initialized, a second step is often to immediately assign an instance of the class that’s adopted the delegate protocol to its delegate property so that everything is “wired up”.

### Adopt and implement the Delegate Protocol
In our delegate class we tell the compiler that we are adopting the protocol and then implement the delegate method(s). In Objective-C we can use a category to declare the protocol conformance:

```
@interface MasterViewController () <DetailViewControllerDelegateProtocol>
@end

- (void)didFinishTask:(DetailViewController *)sender {
  // do stuff like updating the UI
}
```

For the Swift equivalent code we can use an extension to the master view controller class to declare and implement the delegate function:

```
extension MasterViewController: DetailViewControllerDelegate {
  func didFinishTask(sender: DetailViewController) {
    // do stuff like updating the UI
  }
}
```
We also need to remember to set the delegate somewhere. This might be in a prepareForSegue or target-action method that will present the detail view controller:

```
// Objective-C
detailViewController.delegate = self;
```

```
// Swift
detailViewController.delegate = self
```

### Related Resources

<http://www.andrewcbancroft.com/2015/03/26/what-is-delegation-a-swift-developers-guide/>

In the real world, the word delegation encapsulates relationship and responsibility. A delegator/principal (noun) would delegate (verb) control or responsibility to another person called a delegate.

It’s a design pattern that shifts responsibility from one class to another, thereby creating a separation of responsibilities and concerns.

It’s also worth noting the scope of the communication that delegation is intended to be used for. Whereas NSNotificationCenter fits the need for one instance to broadcast a message intended for more than one listening instance, delegation fits the scenario where an instance only needs to send a message to a single listener (the delegate).

Another common usage for delegation is to provide a delegate instance the opportunity to customize certain aspects of the delegating instance’s internal state. Once again, clues from a few of Apple’s APIs shed some light on this usage scenario. Let’s look at UITableViewDelegate first:

```
tableView(_:heightForRowAtIndexPath:)
tableView(_:editActionsForRowAtIndexPath:)
tableView(_:indentationLevelForRowAtIndexPath:)
tableView(_:shouldHighlightRowAtIndexPath:)
```

These are all customization-points that a UITableView allows its delegate to have a say in. Some of the methods are so important that the table view can’t display itself unless it gets this information from its delegate. The point here is that the table view is shifting responsibility for the implementation of that logic off to its delegate, allowing for greater controlled flexibility and customization.

Another article on delegates and protocols
<https://www.iphonelife.com/blog/31369/swift-programming-101-mastering-protocols-and-delegates-part-2>

<https://code.tutsplus.com/tutorials/swift-from-scratch-delegation-and-properties--cms-23445>

### Differences between Java interfaces and Objective-C protocols?

<http://stackoverflow.com/questions/990360/differences-between-java-interfaces-and-objective-c-protocols>

```
Stackoverflow opinion:
They are almost identical. However the one thing that has caught me out, is that unless you explicitly declare that an objective C protocol also implements NSObject, references to that protocol don't get access to the methods that NSObject declares (without a compiler warning anyway). With java you can have a reference to an interface, and still call toString() etc on it.
```

### Brian Summary:
```
A delegator class gives control or responsibility over to another class called a delegate for some behaviorial logic. In iOS and Swift this is done by an abstraction layer is known as a protocol.
```

## Polymorphism

<https://en.wikipedia.org/wiki/Polymorphism_(computer_science)>

In programming languages and type theory, polymorphism (from Greek πολύς, polys, "many, much" and μορφή, morphē, "form, shape") is the provision of a single interface to entities of different types.[1]

A polymorphic type is one whose operations can also be applied to values of some other type, or types.[2] There are several fundamentally different kinds of polymorphism:

* Ad hoc polymorphism: when a function denotes different and potentially heterogeneous implementations depending on a limited range of individually specified types and combinations. Ad hoc polymorphism is supported in many languages using function overloading.
* Parametric polymorphism: when code is written without mention of any specific type and thus can be used transparently with any number of new types. In the object-oriented programming community, this is often known as generics or generic programming. In the functional programming community, this is often shortened to polymorphism.
* Subtyping (also called subtype polymorphism or inclusion polymorphism): when a name denotes instances of many different classes related by some common superclass.[3] In the object-oriented programming community, this is often referred to as simply polymorphism.

## Protocol Oriented Programming

WWDC 2015 - Session 408: Protocol Oriented Programming
<https://developer.apple.com/videos/play/wwdc2015/408/>

<http://clean-swift.com/protocol-oriented-principle/>

```
When I first learned about polymorphism in C++, it was at the function level. The goal is to avoid having to duplicate functions that take different types of parameters but do the same thing, making code reuse possible.
```
### Duplication at the function level

```
Swift

struct Professor
{
  var facultyID: String
  var name: String
  var phone: String
  var department: String
}

struct Student
{
  var studentID: String
  var name: String
  var phone: String
  var major: String
}

func call(professor: Professor)
{
  debugPrint("Calling professor \(professor.name) at \(professor.phone)")
}

func call(student: Student)
{
  debugPrint("Calling student \(student.name) at \(student.phone)")
}

let steve = Professor(facultyID: "123", name: "Steve", phone: "123-456-7890", department: "English")
let tim = Student(studentID: "456", name: "Tim", phone: "987-654-3210", major: "Math")

call(steve)
call(tim)
```
It should be possible to call a professor or a student. But two call() functions are required because steve is a professor and tim is a student. Although they both have a phone number, they are of different types.

### Inheritance at the function level

```
class Person
{
  var name: String
  var phone: String

  init(name: String, phone: String)
  {
    self.name = name
    self.phone = phone
  }
}

class Professor: Person
{
  var facultyID: String
  var department: String

  init(facultyID: String, name: String, phone: String, department: String)
  {
    self.facultyID = facultyID
    self.department = department
    super.init(name: name, phone: phone)
  }
}

class Student: Person
{
  var studentID: String
  var major: String

  init(studentID: String, name: String, phone: String, major: String)
  {
    self.studentID = studentID
    self.major = major
    super.init(name: name, phone: phone)
  }
}

func call(person: Person)
{
  debugPrint("Calling \(person.phone)")
}

let steve = Professor(facultyID: "123", name: "Steve", phone: "123-456-7890", department: "English")
let tim = Student(studentID: "456", name: "Tim", phone: "987-654-3210", major: "Math")

call(steve)
call(tim)
```
Professor and Student are changed to classes and inherit from Person. The shared attributes name and phone are now in the base class.

The call() function takes a Person object as its parameter. The function body can simply invoke person.phone.

You can invoke the call() function and pass in either a Professor or Student. The reason it works is because Professor and Student inherit from the same Person base class. At runtime, the object is cast to a Person object to be accessed in the body.

### Inheritance at the entity level

So far, you’ve seen polymorphism at the function level. You avoid having to duplicate the call() function by using inheritance and base class pointer.

But the same principle can also be applied at the entity level.

```
class Person
{
  var name: String
  var phone: String

  init(name: String, phone: String)
  {
    self.name = name
    self.phone = phone
  }

  func call(person: Person)
  {
    debugPrint("Calling \(person.phone)")
  }
}

class Professor: Person
{
  var facultyID: String
  var department: String

  init(facultyID: String, name: String, phone: String, department: String)
  {
    self.facultyID = facultyID
    self.department = department
    super.init(name: name, phone: phone)
  }
}

class Student: Person
{
  var studentID: String
  var major: String

  init(studentID: String, name: String, phone: String, major: String)
  {
    self.studentID = studentID
    self.major = major
    super.init(name: name, phone: phone)
  }
}

let steve = Professor(facultyID: "123", name: "Steve", phone: "123-456-7890", department: "English")
let tim = Student(studentID: "456", name: "Tim", phone: "987-654-3210", major: "Math")

steve.call(tim)
tim.call(steve)
```

I’ve moved the call() function to the Person class definition. You now invoke the call method on a Person object. After all, a person calls another person, right?

Class inheritance allows you to extract common behavior to a base class.

### Protocol at the entity level

Over time, we’ve discovered inheritance comes with its own [problems](http://blogs.perl.org/users/sid_burn/2014/03/inheritance-is-bad-code-reuse-part-1.html) - see "Problems with Inheritance" section below.

Two subclasses may share some common behaviors but not a lot. As a result, the base class becomes small and not very useful because you can’t put a lot of common behaviors in it. On the other hand, if you try to put more common behaviors in it, you’ll end up having to override the default behaviors in your subclasses.

Another problem occurs when your application grows. You need to model new objects. Expanding your class hierarchy to accommodate these new classes can also quickly turn into a nightmare. You may need to introduce new superclasses and regroup your subclasses. This means existing applications that use your class hierarchy will be completely broken when they are upgraded to use your new class hierarchy.

You often hear composition is preferred over inheritance. Protocols in Swift allow you to "compose different behaviors" in your classes.

(For an object oriented programming language such as C++, it allows a subclass to inherit from multiple superclasses. This is called multiple inheritance. And you can use it to simulate protocols. This mix-and-match concept is the same.)

```
protocol Callable
{
  var phone: String { get set }
  func call(callee: Callable)
}

struct Professor: Callable
{
  var facultyID: String
  var name: String
  var phone: String
  var department: String

  func call(callee: Callable)
  {
    debugPrint("Calling \(callee.phone)")
  }
}

struct Student: Callable
{
  var studentID: String
  var name: String
  var phone: String
  var major: String

  func call(callee: Callable)
  {
    debugPrint("Calling \(callee.phone)")
  }
}

let steve = Professor(facultyID: "123", name: "Steve", phone: "123-456-7890", department: "English")
let tim = Student(studentID: "456", name: "Tim", phone: "987-654-3210", major: "Math")

steve.call(tim)
tim.call(steve)
```

```
Brian: The function call: func call(callee: Callable) was throwing me because I did not understand why the call method parameter was of type Callable. But this simply means an object that implements the Callable interface which makes the properties and methods available to the call function.
```

### Extension at the entity level

Protocols are great because they allow you to customize behaviors while not inheriting the hindrance from the parent.

But we’re back at our original problem with duplication. The call methods for both Professor and Student are the same. Wouldn’t it be nice to remove this duplication?

```
protocol Callable
{
  var phone: String { get set }
  func call(callee: Callable)
}

extension Callable
{
  func call(callee: Callable)
  {
    debugPrint("Calling \(callee.phone)")
  }
}

struct Professor: Callable
{
  var facultyID: String
  var name: String
  var phone: String
  var department: String
}

struct Student: Callable
{
  var studentID: String
  var name: String
  var phone: String
  var major: String
}

let steve = Professor(facultyID: "123", name: "Steve", phone: "123-456-7890", department: "English")
let tim = Student(studentID: "456", name: "Tim", phone: "987-654-3210", major: "Math")

steve.call(tim)
tim.call(steve)
```

You can provide an extension to the Callable protocol to define the default behavior of the call() method. Professor and Student can simply use the default call() method if that’s sufficient. Or, they can define a customized version of the call() method.

A real life use case can be local vs long-distance calling. A default call() method can implement the details to make a local area call to another person. But a customized call() method can have extra instructions to process payment.

### Generics at the function level

This is great for object oriented programming languages. But functional programming is the new buzz now. To me, functional programming isn’t new. It’s been around for ages. It preceded object oriented programming.

Let’s see how we can combine protocols and generics to achieve polymorphism back at the function level.

```
protocol Callable
{
  var phone: String { get set }
}

struct Professor: Callable
{
  var facultyID: String
  var name: String
  var phone: String
  var department: String
}

struct Student: Callable
{
  var studentID: String
  var name: String
  var phone: String
  var major: String
}

func call<T: Callable>(callee: T)
{
  debugPrint("Calling \(callee.phone)")
}

let steve = Professor(facultyID: "123", name: "Steve", phone: "123-456-7890", department: "English")
let tim = Student(studentID: "456", name: "Tim", phone: "987-654-3210", major: "Math")

call(steve)
call(tim)

```

Here, the Callable protocol only requires a phone number. And we’ve changed Professor and Student back to structs. In Swift, structs can conform to protocols like classes. They conform to Callable by defining the phone variable.

Next, we define the call() function to take a generic parameter that confirms to the Callable protocol. In the method body, you can invoke callee.phone.

Finally, we’re back at solving our original problem – avoiding function duplication.

In conclusion, writing reusable code without duplication is at the heart of good software design. Pointers, inheritance, interfaces, protocols, and generics are features provided by programming languages to achieve the same goal – polymorphism.

### The generics advantages (Updated)

Doug commented below and asked what the advantages are for using Swift generics for the call() function above. In this specific case, there’s none. I was just trying to illustrate how you could use generics with the simple Callable protocol to implement polymorphism in Swift.

But that wouldn’t be a very good example, isn’t it? So here’s an expanded version.

Your Callable protocol can be more complex. For example, if you want to allow a phone number to be a String or Int because you want people to be able to dial 123-456-7890 or 1234567890. You’ll then need to use associatedtype in your Callable protocol. The PhoneNumber associated type acts as a placeholder for the type of the phone number, which can be a string or integer.

Furthermore, you can also specify protocol conformance constraints for your associated type such that T.PhoneNumber conforms to the CustomDebugStringConvertible protocol. Your phone number can then be printed with dashes regardless if it’s a string or integer.

```
// MARK: - Generics at the function level

import Foundation

extension Int: CustomDebugStringConvertible
{
  public var debugDescription: String {
    var phone = String(self)
    phone.insert("-", atIndex: phone.startIndex.advancedBy(3))
    phone.insert("-", atIndex: phone.startIndex.advancedBy(7))
    return phone
  }
}

protocol Callable
{
  associatedtype PhoneNumber
  var phone: PhoneNumber { get set }
}

struct Professor: Callable
{
  var facultyID: String
  var name: String
  var phone: String
  var department: String
}

struct Student: Callable
{
  var studentID: String
  var name: String
  var phone: Int
  var major: String
}

func call<T: Callable where T.PhoneNumber: CustomDebugStringConvertible>(callee: T)
{
  debugPrint(callee.phone)
}

// Error: Protocol ‘Callable’ can only be used as a generic constraint because it has Self or associated type requirements
func call(callee: Callable)
{
  print("Phoning \(callee.phone)")
}

let steve = Professor(facultyID: "123", name: "Steve", phone: "123-456-7890", department: "English")
let tim = Student(studentID: "456", name: "Tim", phone: 9876543210, major: "Math")

call(steve)
call(tim)
```

The non-generic version of the call() method doesn’t compile because the Callable protocol uses the associated type PhoneNumber. As a result, you’ll need to implement the call() method using generics.

Swift generics can be very powerful. But like Doug said, it shouldn’t be used unnecessarily.

### Generics - Associated Types

<https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Generics.html>

When defining a protocol, it is sometimes useful to declare one or more associated types as part of the protocol’s definition. An associated type gives a placeholder name to a type that is used as part of the protocol. The actual type to use for that associated type is not specified until the protocol is adopted. Associated types are specified with the associatedtype keyword.

### Generic Where Clauses

<https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Generics.html#//apple_ref/doc/uid/TP40014097-CH26-ID192>

It can also be useful to define requirements for associated types. You do this by defining a generic where clause. A generic where clause enables you to require that an associated type must conform to a certain protocol, or that certain type parameters and associated types must be the same. A generic where clause starts with the where keyword, followed by constraints for associated types or equality relationships between types and associated types. You write a generic where clause right before the opening curly brace of a type or function’s body.

The example below defines a generic function called allItemsMatch, which checks to see if two Container instances contain the same items in the same order. The function returns a Boolean value of true if all items match and a value of false if they do not.

The two containers to be checked do not have to be the same type of container (although they can be), but they do have to hold the same type of items. This requirement is expressed through a combination of type constraints and a generic where clause:

```
func allItemsMatch<C1: Container, C2: Container>
    (_ someContainer: C1, _ anotherContainer: C2) -> Bool
    where C1.ItemType == C2.ItemType, C1.ItemType: Equatable {

        // Check that both containers contain the same number of items.
        if someContainer.count != anotherContainer.count {
            return false
        }

        // Check each pair of items to see if they are equivalent.
        for i in 0..<someContainer.count {
            if someContainer[i] != anotherContainer[i] {
                return false
            }
        }

        // All items match, so return true.
        return true
}
```

This function takes two arguments called someContainer and anotherContainer. The someContainer argument is of type C1, and the anotherContainer argument is of type C2. Both C1 and C2 are type parameters for two container types to be determined when the function is called.

The following requirements are placed on the function’s two type parameters:

C1 must conform to the Container protocol (written as C1: Container).
C2 must also conform to the Container protocol (written as C2: Container).
The ItemType for C1 must be the same as the ItemType for C2 (written as C1.ItemType == C2.ItemType).
The ItemType for C1 must conform to the Equatable protocol (written as C1.ItemType: Equatable).

## Generics Explained

<https://www.tutorialspoint.com/swift/swift_generics.htm>

* Generic Functions: Type Parameters
* Generic Types
* Extending a Generic Type
* Associated Types
* Where Clauses

## Other Resources for Protocol Oriented Programming

<https://www.raywenderlich.com/148448/introducing-protocol-oriented-programming>

### Apple documentation:

<https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Protocols.html>

#### Protocol Syntax

You define protocols in a very similar way to classes, structures, and enumerations:

```
protocol SomeProtocol {
    // protocol definition goes here
}
```

Custom types state that they adopt a particular protocol by placing the protocol’s name after the type’s name, separated by a colon, as part of their definition. Multiple protocols can be listed, and are separated by commas:

```
struct SomeStructure: FirstProtocol, AnotherProtocol {
    // structure definition goes here
}
```
### Delegation & Protocols

Delegation is a design pattern that enables a class or structure to hand off (or delegate) some of its responsibilities to an instance of another type. This design pattern is implemented by defining a protocol that encapsulates the delegated responsibilities, such that a conforming type (known as a delegate) is guaranteed to provide the functionality that has been delegated. Delegation can be used to respond to a particular action, or to retrieve data from an external source without needing to know the underlying type of that source.

The example below defines two protocols for use with dice-based board games:

```
protocol DiceGame {
    var dice: Dice { get }
    func play()
}

protocol DiceGameDelegate {
    func gameDidStart(_ game: DiceGame)
    func game(_ game: DiceGame, didStartNewTurnWithDiceRoll diceRoll: Int)
    func gameDidEnd(_ game: DiceGame)
}
```

The DiceGame protocol is a protocol that can be adopted by any game that involves dice. The DiceGameDelegate protocol can be adopted by any type to track the progress of a DiceGame.

## Problems with Inheritance

### Inheritance is Bad Article

From the following article (that seems to go along with the WWDC 2015 Seesion 408) (but the author's English is second rate) -
<http://blogs.perl.org/users/sid_burn/2014/03/inheritance-is-bad-code-reuse-part-1.html>

#### Code Reuse

" a lot of people try to archive Code Reuse through inheritance."

"Put your code in your super class and inherit from that class. Now you can create your new class that also inherit from that super class. That concept sounds really easy."

So easy, that often other ways to archive code reuse will never shown, and a lot of programmers don't even think of solving it another way. Just for example pick the "Object Oriented Perl" Book from Damian Conway. It's just a book about Object Oriented Programming in Perl, over 400 pages. And something like "Composition" and "Aggregation" is only mentioned as a site-node.

#### What's wrong with inheritance

<http://blogs.perl.org/users/sid_burn/inheritance4.png>

Now we have some inheritance graph that looks somewhat usable. We can now creat a lot of Birds that now can inherit from "Bird". And they all have in common that they can fly. We can add Lions, Zebra, Monkeys and so on to the "Mammal" Class. And then we can add our Dolphins, Sharks, Octopuses to the Fish Category because they swim.

Well Dolphins are mammals too, but we still put them under Fish, because Dolphins Swim. I mean, don't be so nitpicky right? What we really want is that our Dolphin gets our Swim Code, not that every word is exactly correct, right? Yes i knew Octopuses are also not fishes, but that is just nitpicky. The important part is that our Octopus also can swim, and we used proper Code Reuse, right?

So lets add some other animals. I like Penguins. So lets add a Penguin. Hmm, where do we put it? A Penguin is Bird right? But well, a Penguin can't fly even the fact that he has wings. So we put it under Fish right? Because a Penguin can swim. But wait, a penguin can also walk. So actually we need a new subclass once again! Oh darn...

Okay, now we have to implement Flying Fishes. Oh darn, that think can swim and fly? Crocodiles that also swim and walk? And now we also want to implement the "American Dipper". What is an American Dipper? Oh seriously? A bird that "flys" under water or better swims? But still walks and fly? Really, who created all that mess?

#### Inheritance recap

Okay, now jokes aside. But i hope you start seeing the problem with inheritance. We usally create "abstract classes" and put code in them. And with "abstract" i don't mean "abstract classes" like in Java oder C#. What i mean is abstract that we name it "Bird", "Mammal", "Fish" or something else. And what do we do with it? We "link" specific behaviours to that abstract class names. For example we say that a Bird can Fly. So everything that should fly needs to be a bird.

But the problem that we sooner get is that we have some animals that also have the behaviour "Fly", but they are not Birds. For example the flying fishes. Even a "bat" is not a bird. Birds have "Feather". And also birds are not mammals. But we have mammals that still can fly like the mentioned "bat".

If you look at it then it just seems extremely complicated, but just look what we wanted to do. We just wanted to implement different Animals. And the only thing we wanted was to describe that some Animals can swim, fly or walk. So why is this such a big problem?

Well because instead of describing exactly that "A Penguin can walk and swim" we tried to invent some abstract classes like "Bird". And then we suggested that every Bird can fly, but that is not the case. The problem is, we "link abstract names with exact behaviours". And that is just wrong. Yes, 90% of the birds can fly, but not every bird. And that is why we run in problems.

But lets rethink. Why do we do all that stuff? What was the purpose? Did we wanted to describe that a penguin is a bird? Heck no, nobody wanted that. Our intention was to have Code Reuse. We wanted just to write our Flying code, and we wanted to reuse our code so that we don't have to rewrite it again.

But the big question is, why do we try to think abstract with Birds, Mammals, Animals or Fishes. Why do we not just Code the "Behaviours" directly?

#### Behaviours

Okay, so now we knew its better to think in Behaviours. We should Code something and describe exactly what it does. So we have Code that implements "Flying". Then just name it "Fly", "Flying", "Flyable" or whatever name you come up with. But name it "Fly", not "Bird". So we have three things we want to reuse. Flying, Walking and Swiming. And now lets say we want to Describe our Dove, Tiger, Penguin, Goldfisch, Flying Fish, Bat.

Dove -> Flying, Walking
Tiger -> Walking
Penguin -> Walking, Swiming
Goldfish -> Swiming
Flying Fish -> Swiming, Flying
Bat -> Flying

So, is that description above anywhere complex, not understandable? I think it is by far more easier then trying to put in an inheritance graph. You just name the behaviours what exactly they do, and put only that code inside this behaviour. And even if you want to add something. A Bat can also walk/swim? Just add that behaviour to the bat. Done. And now lets even think further. We want to implement the T-800, submarine, tank and a helicopter

T-800 -> Walking
Submarine -> Swiming
Tank -> Walking
Helicopter -> Flying

Any Problem? Where would you put your Helicopter in your inheritance graph? Under Bird? But one question is still open. How do you implement these Behaviours exactly?

#### Implementation

There exists two ways how to implement Behaviours. The first way is, if you "abuse" multiple inheritance with it. You just create three classes and name them after your behaviour "Flying", "Walking" and "Swiming". Then you are able to just reuse your classes as much as possible. Now you can just create your Penguin class and inherit from your "Walking" and your "Swiming" class.

Well, now we use inheritance again, but the important thing is to make clear how you use inheritance. What you are doing is using classes in two ways. Some classes are just "templates" or a way to achieve your code reuse. But these classes will never be used directly. For example you never create an object directly from the Flying class. Another important thing is that these templates are named after your behaviour. And even more important. Every template should only implement one Behaviour.

But a better way that makes it clear that you have reusable code is to use Roles instead. A Role does exactly that. Think of behaviours. Put your code in a Role. Now you cannot inherit from that Role, you cannot create an object from that Role. But you can insert that Role in another class.

And using Roles will also help you in thinking of different "Behaviours". And it will help you, because everytime when you inherit from something you think instinctively: "Oh wait, why do i want to inherit from it?" And often you see that you just want to reuse some code somewhere that you better put in an Role.

It will also help you to faster understand what a class is capable of. Because everything is flat. You just have your class, and your class have a list of Roles. You don't have a Penguin that inherits from "Bird" and/or "Fish". And that Bird inherit again from mammal that inherits from Animal and so on.

So if you think in behaviours and use roles for code reuse you will get a much easier and much more powerful code base. You can easily reuse code. And you can reuse code in a way that you probably never thought when you started your project. Like the helicopter that also can fly and don't needs to inherit from a "Bird" class or something like that.

But i also want to mention that roles are not the holy grail. They are just another way to achieve code reuse, in my opinion it is a better way as to use inheritance, but also roles have some problems. In my next Blog i will talk about the problems with Roles. But it is still important that the concept about "behaviours" will not change. In the next Blog i will show you how you can achieve an even higher level of code reuse that is even more flexible without roles.

## Generics

<https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Generics.html>

Generic code enables you to write flexible, reusable functions and types that can work with any type, subject to requirements that you define. You can write code that avoids duplication and expresses its intent in a clear, abstracted manner.

Generics are one of the most powerful features of Swift, and much of the Swift standard library is built with generic code. In fact, you’ve been using generics throughout the Language Guide, even if you didn’t realize it. For example, Swift’s Array and Dictionary types are both generic collections. You can create an array that holds Int values, or an array that holds String values, or indeed an array for any other type that can be created in Swift. Similarly, you can create a dictionary to store values of any specified type, and there are no limitations on what that type can be.

### Generics Syntax	
From [here](http://waynewbishop.com/swift/generics/):
The syntax <T> seen after the class name is called a placeholder. With generics, values seen inside angled brackets (e.g., T ) are declared variables. Once the placeholder T is established, it can be reused anywhere a class reference would be expected. - See more at: http://waynewbishop.com/swift/generics/#sthash.FE5YGghe.dpuf

### The Problem That Generics Solve

Here’s a standard, non-generic function called swapTwoInts(_:_:), which swaps two Int values:

```
func swapTwoInts(_ a: inout Int, _ b: inout Int) {
    let temporaryA = a
    a = b
    b = temporaryA
}
```

This function makes use of in-out parameters to swap the values of a and b, as described in In-Out Parameters.

The swapTwoInts(_:_:) function swaps the original value of b into a, and the original value of a into b. You can call this function to swap the values in two Int variables:

```
var someInt = 3
var anotherInt = 107
swapTwoInts(&someInt, &anotherInt)
print("someInt is now \(someInt), and anotherInt is now \(anotherInt)")
// Prints "someInt is now 107, and anotherInt is now 3"
```

The swapTwoInts(_:_:) function is useful, but it can only be used with Int values. If you want to swap two String values, or two Double values, you have to write more functions, such as the swapTwoStrings(_:_:) and swapTwoDoubles(_:_:) functions shown below:

```
func swapTwoStrings(_ a: inout String, _ b: inout String) {
    let temporaryA = a
    a = b
    b = temporaryA
}

func swapTwoDoubles(_ a: inout Double, _ b: inout Double) {
    let temporaryA = a
    a = b
    b = temporaryA
}
```
You may have noticed that the bodies of the swapTwoInts(_:_:), swapTwoStrings(_:_:), and swapTwoDoubles(_:_:) functions are identical. The only difference is the type of the values that they accept (Int, String, and Double).

It would be much more useful, and considerably more flexible, to write a single function that could swap two values of any type. Generic code enables you to write such a function. (A generic version of these functions is defined below.)

NOTE

In all three functions, it is important that the types of a and b are defined to be the same as each other. If a and b were not of the same type, it would not be possible to swap their values. Swift is a type-safe language, and does not allow (for example) a variable of type String and a variable of type Double to swap values with each other. Attempting to do so would be reported as a compile-time error.

Generic Functions

Generic functions can work with any type. Here’s a generic version of the swapTwoInts(_:_:) function from above, called swapTwoValues(_:_:):
```
func swapTwoValues<T>(_ a: inout T, _ b: inout T) {
    let temporaryA = a
    a = b
    b = temporaryA
}
```

The body of the swapTwoValues(_:_:) function is identical to the body of the swapTwoInts(_:_:) function. However, the first line of swapTwoValues(_:_:) is slightly different from swapTwoInts(_:_:). Here’s how the first lines compare:

```
func swapTwoInts(_ a: inout Int, _ b: inout Int)
func swapTwoValues<T>(_ a: inout T, _ b: inout T)
```

The generic version of the function uses a placeholder type name (called T, in this case) instead of an actual type name (such as Int, String, or Double). The placeholder type name doesn’t say anything about what T must be, but it does say that both a and b must be of the same type T, whatever T represents. The actual type to use in place of T will be determined each time the swapTwoValues(_:_:) function is called.

The other difference is that the generic function’s name (swapTwoValues(_:_:)) is followed by the placeholder type name (T) inside angle brackets (<T>). The brackets tell Swift that T is a placeholder type name within the swapTwoValues(_:_:) function definition. Because T is a placeholder, Swift does not look for an actual type called T.

The swapTwoValues(_:_:) function can now be called in the same way as swapTwoInts, except that it can be passed two values of any type, as long as both of those values are of the same type as each other. Each time swapTwoValues(_:_:) is called, the type to use for T is inferred from the types of values passed to the function.

In the two examples below, T is inferred to be Int and String respectively:

```
var someInt = 3
var anotherInt = 107
swapTwoValues(&someInt, &anotherInt)
// someInt is now 107, and anotherInt is now 3

var someString = "hello"
var anotherString = "world"
swapTwoValues(&someString, &anotherString)
// someString is now "world", and anotherString is now "hello"
```

More:

<https://www.tutorialspoint.com/swift/swift_generics.htm>

<http://apple-swift.readthedocs.io/en/latest/Generics.html> Swift 2.2 on Generics

## Blocks / Closures

<https://www.lynda.com/Swift-tutorials/Work-closures/517449/564648-4.html>

Functions in Swift are actually just named closures. Closures are blocks of executable code that can be called in line or stored as a value and passed around in code to be executed at a later time. Many programming languages include closures, sometimes called blocks, Lambdas or anonymous function.

If an inline closure is the last parameter of a function also called a trailing closure it can be moved outside of the parenthesis. And if the function does not take any other required parameters they empty parenthesis can be omitted all together. Next if the parameter types can be inferred you can remove their explicit type declaration and even the parenthesis enclosing the parameter list. Same goes for the return type. If it can be inferred you can omit writing it, but you still have to write the in keyword.

And finally using a closures automatically assigned shorthand argument names you can omit explicitly declaring the parameter list and then even the in keyword. These shorthand argument names begin with dollar zero, then dollar one etcetera in the order defined. You might agree that these changes produce a cleaner syntax perhaps at the expense of intuitiveness if you didn't know about these syntax shortcuts, but you will see the syntax often so it's good to know about it.

## Collection types

Swift defines three formal collection types, array, dictionary, and set. 

## Sets

Sets: And finally using a closures automatically assigned shorthand argument names you can omit explicitly declaring the parameter list and then even the in keyword. These shorthand argument names begin with dollar zero, then dollar one etcetera in the order defined. You might agree that these changes produce a cleaner syntax perhaps at the expense of intuitiveness if you didn't know about these syntax shortcuts, but you will see the syntax often so it's good to know about it.

## Tuples

Tuples in Swift can be used to group multiple values into a single compound value. The group values can be of different types and the values are ordered.

## Computed Properties

<https://www.natashatherobot.com/swift-computed-properties/>

A computed property is essentially a function disguised as a property. Here is an example:

```
var width = 100.0
var height = 100.0
 
// this is the computed property!
var area: Double {
    return width * height
}
 
area // 10,000.0
 
width = 10.0
height = 10.0
area // 100.0
 
width = 15.0
height = 10.0
area // 150.0
```

<http://goshdarnblocksyntax.com/>

Swift version of Objective-C blocks is closures:

<https://www.codefellows.org/blog/writing-completion-blocks-with-closures-in-swift/>

Objective C:

```
//setup a simple red view
UIView *redBox = [[UIView alloc] initWithFrame:CGRectMake(0,    0, 100, 100)];
    redBox.backgroundColor = [UIColor redColor];
    [self.view addSubview:redBox];

    [UIView animateWithDuration:1 animations:^{
        redBox.alpha = 0;
    } completion:^(BOOL finished) {
        NSLog(@"Red box has faded out");
    }];
```

Swift:

```
let redBox = UIView(frame: CGRectMake(0, 0, 100, 100))
    redBox.backgroundColor = UIColor.redColor()
    self.view.addSubview(redBox)

    UIView.animateWithDuration(1, animations:  {() in
            redBox.alpha = 0
        }, completion:{(Bool)  in
            println("red box has faded out")
        })
```

Much to the dismay of rabbits and near-sighted developers, carets ( ^ ) are nowhere to be found (yet) in Swift. Rest in peace old friend, you have been replaced by the keyword in. You use the keyword in once you have finished declaring the parameters and the return values for your closure. This will make more sense if I show you what autocomplete filled in once it knew what method I wanted:

```
UIView.animateWithDuration(duration: NSTimeInterval,
    animations: (() -> Void)?,
    completion: ((Bool) -> Void)?)
```

Focusing on this syntax, it becomes clear how to properly write the parameters and return values of a closure:

```
{(parameters) -> (return type) in expression statements}
```

## Tradeoffs between closure/blocks and delegates

<http://blog.stablekernel.com/blocks-or-delegates/>

### Most delegate protocols have a handful of messages.

I'm looking at GKMatch right now. I see a message for when data is received from another player, when the player changed state, when there is an error and when a player should be re-invited. These are all distinct events. If Apple were to use closures here, they'd have two options. One, they could register a closure for each event. If someone writes a class that does this in Swift, they are probably not smart.
Thus, we can say that "If an object has more than one distinct event, use delegation."

### An object can only have one delegate.

Since an object can only have one delegate, it can really only talk to that one delegate and nobody else. Let's look at CLLocationManager here. The location manager will tell one object (and only one object) when a location is found. Of course, if we need more than one object to know about these updates, we'd probably create another location manager.

What if CLLocationManager were a singleton, though? If we couldn't create any other instances of CLLocationManager, we'd constantly have to be swapping the delegate pointer to whichever object wanted location data. (Or set up some elaborate broadcast system which you - and only you - will understand.) So, it doesn't make much sense to use delegation for a singleton.

The best example of this is UIAccelerometer. In earlier versions of iOS, the singleton accelerometer instance had a delegate that we had to swap occasionally. This was stupid enough that it was changed in later versions of iOS. Nowadays, any object can attach a closure to the CMMotionManager without preventing another object from receiving updates.

Here we can say that, "If an object is a singleton, we can't use delegation."

### Some delegate methods expect a return value

If you look at some delegate methods (and nearly all dataSource methods), there is an expected return value. That means the delegating object is asking for the state of something. While a closure could reasonably maintain state or at least deduce state, this is really an object's role.

Think about it. If I ask a closure "What do you think about Bob?" it can only do two things: send a message to a captured object asking what the object thinks about Bob or return a captured value. If it's returning the response of an object, we should just bypass the closure and go right to the object. If it's returning a captured value, why isn't that a property on the object?

From this observation, we can say that "If the object is calling back for additional information, we'll probably use delegation."

### Process vs. Results

If I look at NSURLConnectionDelegate and NSURLConnectionDataDelegate, I see messages that say things like "I'm starting to do this," "Here is what I know so far," "I've finished doing this," or "DEAR GOD THE WORLD IS ENDING, DEALLOC! DEALLOC! DEALLOC!" These messages outline a process where an interested delegate will want to be informed at each step.

When I look at handler and completion methods, I see a closure that contains a response object and an error object. There isn't any communication for "Here is where I am so far, and I'm still working."

If I look at NSURLConnectionDelegate and NSURLConnectionDataDelegate, I see messages that say things like "I'm starting to do this," "Here is what I know so far," "I've finished doing this," or "DEAR GOD THE WORLD IS ENDING, DEALLOC! DEALLOC! DEALLOC!" These messages outline a process where an interested delegate will want to be informed at each step.

When I look at handler and completion methods, I see a closure that contains a response object and an error object. There isn't any communication for "Here is where I am so far, and I'm still working."

Thus, we can say that delegate callbacks are more process oriented and closure are more results oriented. If you need to be informed along the way of a multi-step process, you'll probably want to use delegation. If you just want the information you are requesting (or details about a failure to get the information), you should use a closure. (If you combine this with item 3 in this list, you'll realize that the delegate can maintain state across all of these events whereas multiple stand-alone closures could not.)

### Speed (maybe?)

AVPlayer has a callback for when the current playback time changes. This sounds more like a process than a result, so by observation 4, we'd use delegation. But this particular callback uses a closure. I'm guessing this is for speed - since this closure can be called, theoretically, hundreds of times per second, the message lookup might be slow.

# Understand when it’s reasonable to swizzle methods

<https://blog.newrelic.com/2014/04/16/right-way-to-swizzle/>

Swizzling is the act of changing the functionality of a method by replacing the implementation of that method with another, usually at runtime. There are many different reasons one might want to use swizzling: introspection, overriding default behavior, or maybe even dynamic method loading. I’ve seen a lot of blog posts discussing swizzling in Objective-C, and a lot of them recommend some pretty bad practices. These bad practices aren’t really a big deal if you’re writing standalone applications, but if you’re writing frameworks for third-party developers, swizzling can mess up some basic assumptions that keep everything running smoothly. So what is the right way to swizzle in Objective-C?

Let’s start with the basics. When I say swizzling I mean the act of replacing the original method with my own method, and usually, calling the original method from within the replacement method. Objective-C permits this practice with the functions provided in the Objective-C Runtime.

In conclusion, to avoid conflicting with other third-party SDKs, don’t swizzle using Objective-C methods and method_swapImplementations(), but instead use C functions and method_setImplementation(), casting these C functions as IMPs. This avoids all the extra information baggage that comes along with an Objective-C method, such as a new selector name. If you want to swizzle, the best outcome is to leave no trace.

<http://nshipster.com/method-swizzling/>

Lots of negatives, only see one positive besides no other choice (Steve Roberts)

<http://stackoverflow.com/questions/5339276/what-are-the-dangers-of-method-swizzling-in-objective-c>

Method swizzling can be very helpful is in unit testing.

<https://wiredcraft.com/blog/method-swizzling-ios/>

Method Swizzling is a dynamic feature that can exchange the implementations of two methods in runtime. This means it can implement some complex functions conveniently, however, it can also be dangerous to use because its scope can change the class of the project and can cause fatal errors for the project if done wrong.

Here are some pitfalls of Method Swizzling. Pay attention to these when you use Method Swizzling in your projects.

* Method swizzling is not atomic
* Changes behavior of un-owned code
* Possible naming conflicts
* Swizzling changes the method’s arguments
* The order of swizzles matters
* Difficult to understand (looks recursive)
* Difficult to debug
*
I needed to set a unified background color for all of views in the project. At the beginning, I wrote the following code in every view controller’s’ viewDidLoad method:

```
// setup global background color
self.view.backgroundColor = WXGGlobalBackgroundColor;
```

In this project, there were tons of views and controllers, so I found myself writing this code again and again. It was stupid and inefficient! I knew I needed to find a better solution for this. So, I began to think about Inheritance and Method Swizzling.

Why didn’t I use Inheritance?

As we know, the UIKit framework has its own inherited hierarchy and Objective-C doesn’t support Multiple Inheritance, so if you want to set all views in the project, you cannot just create a subclass of UIViewController, maybe you need do more on UITableViewController and UINavigationController which is already a subclass of UIViewController. The result is that you increase the complexity of the inheritance structure and get no additional benefits.

There are a lot of challenges in making sure everyone on a project knows how to use the custom subclass, because you need to provide documentation for each person on the project and must ensure the documentation is understood and clear when new team members enter the project.

Why did Method Swizzling work better for me?

With Method Swizzling you can change the default implement of UIViewController’s viewDidLoad method, all views in the project will change whatever it belongs to a UINavigationController or a UITableViewController. It is amazing! You just need to change one place and the whole project will be changed.

We can’t give up eating for fear of choking, so the best choice is to keep trying with the Method Swizzling until you understand what’s going on and know how to fix it when it has bugs. Don’t give up. Then you will find that it’s a very useful technique for you to develop iOS apps.

## Behavior Driven Development

<https://en.wikipedia.org/wiki/Behavior-driven_development>

BDD does not have any formal requirements for exactly how these user stories must be written down, but it does insist that each team using BDD come up with a simple, standardized format for writing down the user stories which includes the elements listed above.[2][12] However, in 2007 Dan North suggested a template for a textual format which has found wide following in different BDD software tools.[12] A very brief example of this format might look like this:

```
Story: Returns go to stock

As a store owner
In order to keep track of stock
I want to add items back to stock when they're returned.

Scenario 1: Refunded items should be returned to stock
Given that a customer previously bought a black sweater from me
And I have three black sweaters in stock.
When he returns the black sweater for a refund
Then I should have four black sweaters in stock.

Scenario 2: Replaced items should be returned to stock
Given that a customer previously bought a blue garment from me
And I have two blue garments in stock
And three black garments in stock.
When he returns the blue garment for a replacement in black
Then I should have three blue garments in stock
And two black garments in stock.
```

## Dependency Injection

## iOS Best Practices:

<https://github.com/futurice/ios-good-practices>

* Human Interface Guidelines
* Xcode
* Project Setup

A common question when beginning an iOS project is whether to write all views in code or use Interface Builder with Storyboards or XIB files. Both are known to occasionally result in working software. However, there are a few considerations:

Why code?

Storyboards are more prone to version conflicts due to their complex XML structure. This makes merging much harder than with code.
It's easier to structure and reuse views in code, thereby keeping your codebase DRY (Don't repeat yourself)
All information is in one place. In Interface Builder you have to click through all the inspectors to find what you're looking for.
Storyboards introduce coupling between your code and UI, which can lead to crashes e.g. when an outlet or action is not set up correctly. These issues are not detected by the compiler.

Why Storyboards?

For the less technically inclined, Storyboards can be a great way to contribute to the project directly, e.g. by tweaking colors or layout constraints. However, this requires a working project setup and some time to learn the basics.
Iteration is often faster since you can preview certain changes without building the project.
Custom fonts and UI elements are represented visually in Storyboards, giving you a much better idea of the final appearance while designing.
For size classes, Interface Builder gives you a live layout preview for the devices of your choice, including iPad split-screen multitasking.

Why not both?

To get the best of both worlds, you can also take a hybrid approach: Start off by sketching the initial design with Storyboards, which are great for tinkering and quick changes. You can even invite designers to participate in this process. As the UI matures and reliability becomes more important, you then transition into a code-based setup that's easier to maintain and collaborate on.

* Ignores (.gitignore)
* Dependency Management (cocoapods)
* Project Structure
* Localization
* Constants
* Branching Model (gitFlow?)
* Minimum iOS Version Requirement
* Common Libraries
* AFNetworking/Alamofire
* DateTools
* Auto Layout Libraries
* Event” Patterns

These are the idiomatic ways for components to notify others about things:

Delegation: (one-to-one) Apple uses this a lot (some would say, too much). Use when you want to communicate stuff back e.g. from a modal view.
Callback blocks: (one-to-one) Allow for a more loose coupling, while keeping related code sections close to each other. Also scales better than delegation when there are many senders.
Notification Center: (one-to-many) Possibly the most common way for objects to emit “events” to multiple observers. Very loose coupling — notifications can even be observed globally without reference to the dispatching object.
Key-Value Observing (KVO): (one-to-many) Does not require the observed object to explicitly “emit events” as long as it is Key-Value Coding (KVC) compliant for the observed keys (properties). Usually not recommended due to its implicit nature and the cumbersome standard library API.
Signals: (one-to-many) The centerpiece of ReactiveCocoa, they allow chaining and combining to your heart's content, thereby offering a way out of "callback hell".

* Stores
* Assets
* Coding Style
* Structure
* External Style Guides
* Logging
* Profiling
* Analytics
* Crash Logs (Crashyltics, BuddyBuild, etc)
* Building
* Continuous Integration

## SOLID principles:

    * S – Single-responsiblity principle - A class should have one and only one reason to change, meaning that a class should have only one job.
    * O – Open-closed principle - Objects or entities should be open for extension, but closed for modification.
    * L – Liskov substitution principle - All this is stating is that every subclass/derived class should be substitutable for their base/parent class.
    * I – Interface segregation principle - A client should never be forced to implement an interface that it doesn’t use or clients shouldn’t be forced to depend on methods they do not use.
    * D – Dependency Inversion Principle - Entities must depend on abstractions not on concretions. It states that the high level module must not depend on the low level module, but they should depend on abstractions.

# Understanding Swift Coding Concepts

## General Swift

Basic Swift classes:
<https://classroom.udacity.com/courses/ud902/lessons/4632132281/concepts/46479088550923>

From Lynda.com - Swift 3.0 Essential Training: The Basics

<https://www.lynda.com/Swift-tutorials/Swift-3-Essential-Training-Basics/517449-2.html>

### Swift Data Types

Value and reference types:

![data types](https://gitlab.com/belliott-demo/understanding-swift/raw/master/images/Understand_value_and_reference_types.png)

External and local parameter names for functions:

![parameter names](https://gitlab.com/belliott-demo/understanding-swift/raw/master/images/Functions_External_Local_Names.png)

"Writing the underscore before a parameter name explicitly suppresses displaying that name externally."

### Swift Documentation

Great documentation for Swift types, protocols, operators, and global functions 

<http://swiftdoc.org/>

### Beyond Swift Basics

<https://www.lynda.com/Swift-tutorials/Understand-protocols/546101/577946-4.html>

<http://swiftdeveloperblog.com/code-examples/create-and-customize-uitextview-programmatically-in-swift/>

### Optionals

```
var maybeAString: String?   (the ? makes it an optional so that it can have a value or be nil)
```

Why is this a good thing?

Course explaination:

```
What would happen if I tried to access wordsOfWisdom after it was declared but before it was assigned a value? I'd get an error. Variable wordsOfWisdom used before being initialized. I'll comment that out. Before assigning a value, it is in an uninitialized state.

So surely there must be a way to declare variables that may hold a value or may be nil. There is, optionals. A variable can be declared as an optional by suffixing its type with a question mark. I'll declare a variable maybeAString of type String. If I try to access it before assigning it a value, I'll get an error. But if I change the type to be an optional of string, the error goes away. MaybeAString is nil at this point.

By making maybeAString an optional, I'm saying maybeAString can either contain some value of type string or it is nil. And nil in Swift literally means nothing. It is a valueless state. When you define an optional variable without assigning a default value as I've done here, the variable is automatically set to nil to start out. The question mark is actually a syntax shortcut for declaring the type to be an optional.
```

Apple explanation:

```
<https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html>
"Likewise, type safety prevents you from accidentally passing an optional String to a piece of code that expects a nonoptional String. Type safety helps you catch and fix errors as early as possible in the development process."
```

Other references:

<http://themasterworld.com/how-to-use-optionals-in-swift/>

```In Swift nil means no value```

```NOTE :  nil cannot be used for non optional variables .In any case if you want to assign variable to nil then you have to make that variable as optional```

More explanations:

<http://themasterworld.com/how-to-use-optionals-in-swift/>

If we define any optional variable without any default value then its value will automatically be nil.

```var itWillHaveNilValue : String ?```

### Forced Unwrapping in If statement

As we check the nil value in Objective-C or other similar languages, we do same here in If statement to check that either it contains any value or not any value by using equal to “==” operator and not equal to “!=” operator.


```
if convertNumericStringtoInt  !=  nil {

        print (“ Value is not nil”)
}
```

Once we are sure that variable contains any value then we can access it by adding the exclamation mark at the end of variable. The exclamation mark tell us the “I know this optional definitely has a value please use it.” This is known as Forced Unwrapping. If we will not add any exclamation mark then there will be runtime error.

```
if convertNumericStringtoInt  !=  nil {

       print (“ Value of convertNumericStringtoInt is \(convertNumericStringtoInt!) .”)
}
```

Brian explanation:

```
Optionals are part of Swift's type safety that prevents you from passing an expected value where there is none. By making a value of type String you
are guaranteed there will be a String value. nil cannot be used for non optional variables. So if you want to have a variable that is guaranteed to
have a value, make it a non-optional variable. On the other hand if you want to allow for a variable to not have a value, make it an optional.
```

### Optional Binding in If/While Statement

Like forced unwrapping ,optional binding is another way to to check the value of optional variables and use them.

In optional binding first we check the optional variable that either it contains value of not and if it contains a value then we use it by assign that variable to another temporary constants or variable. This can be used with IF and WHILE statements. The syntax can be written as

```
if let constantName = OptionalVariable {
     //Your Statement
}
```

If you notice here we have not used any exclamation mark on OptionalVariable so our above example can be rewritten as :

```
if  let tempNumber =  Int(numericValueInString) {
      print (“ Value of tempNumber is \(tempNumber) .”)
} else{
     print (“ Value of tempNumber cannot convert to integer .”)
}
```

### Optional chaining

<https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/OptionalChaining.html>

Optional chaining is a process for querying and calling properties, methods, and subscripts on an optional that might currently be nil. If the optional contains a value, the property, method, or subscript call succeeds; if the optional is nil, the property, method, or subscript call returns nil. Multiple queries can be chained together, and the entire chain fails gracefully if any link in the chain is nil.

Example of optional chaining:
<https://www.tutorialspoint.com/swift/swift_optional_chaining.htm>

### Swift Extensions

<https://www.tutorialspoint.com/swift/swift_extensions.htm>

Functionality of an existing class, structure or enumeration type can be added with the help of extensions. Type functionality can be added with extensions but overriding the functionality is not possible with extensions.

Swift Extension Functionalities −

* Adding computed properties and computed type properties.
* Defining instance and type methods.
* Providing new initializers.
* Defining subscripts.
* Defining and using new nested types.
* Making an existing type conform to a protocol.
* Extensions are declared with the keyword 'extension'.

### Swift Class Initialization

<https://www.makeschool.com/online-courses/tutorials/learn-swift-by-example-pt-3-classes-initialization>

## Swift: Struct vs Class

This is the biggest difference between structs and classes. Structs are copied and classes are referenced. ... Thus, they will be passed as values not as a reference in the case of large objects. Swift uses struct in order to improve the performance even with String and Array objects. [ref]<http://stackoverflow.com/questions/24217586/structure-vs-class-in-swift-language>

<https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ClassesAndStructures.html>

<http://stackoverflow.com/questions/24232799/why-choose-struct-over-class/24232845>

According to the very popular WWDC 2015 talk Protocol Oriented Programming in Swift (video, transcript), Swift provides a number of features that make structs better than classes in many circumstances.

Structs are preferable if they are relatively small and copiable because copying is way safer than having multiple reference to the same instance as happens with classes. This is especially important when passing around a variable to many classes and/or in a multithreaded environment. If you can always send a copy of your variable to other places, you never have to worry about that other place changing the value of your variable underneath you.

With Structs there is much less need to worry about memory leaks or multiple threads racing to access/modify a single instance of a variable. (For the more technically minded, the exception to that is when capturing a struct inside a closure because then it is actually capturing a reference to the instance unless you explicity mark it to be copied).

Classes can also become bloated because a class can only inherit from a single superclass. That encourages us to created huge superclasses that encompass many different abilities that are only loosely related. Using protocols, especially with protocol extensions where you can provide implementations to protocols, allows you to eliminate the need for classes to achieve this sort of behavior.

The talk lays out these scenarios where classes are preferred:

* Copying or comparing instances doesn't make sense (e.g., Window)
* Instance lifetime is tied to external effects (e.g., TemporaryFile)
* Instances are just "sinks"--write-only conduits to external state (e.g.CGContext)
* It implies that structs should be the default and classes should be a fallback.

On the other hand, The Swift Programming Language documentation is somewhat contradictory:

Structure instances are always passed by value, and class instances are always passed by reference. This means that they are suited to different kinds of tasks. As you consider the data constructs and functionality that you need for a project, decide whether each data construct should be defined as a class or as a structure.

As a general guideline, consider creating a structure when one or more of these conditions apply:

The structure’s primary purpose is to encapsulate a few relatively simple data values.
It is reasonable to expect that the encapsulated values will be copied rather than referenced when you assign or pass around an instance of that structure.
Any properties stored by the structure are themselves value types, which would also be expected to be copied rather than referenced.
The structure does not need to inherit properties or behavior from another existing type.
Examples of good candidates for structures include:

The size of a geometric shape, perhaps encapsulating a width property and a height property, both of type Double.
A way to refer to ranges within a series, perhaps encapsulating a start property and a length property, both of type Int.
A point in a 3D coordinate system, perhaps encapsulating x, y and z properties, each of type Double.
In all other cases, define a class, and create instances of that class to be managed and passed by reference. In practice, this means that most custom data constructs should be classes, not structures.
