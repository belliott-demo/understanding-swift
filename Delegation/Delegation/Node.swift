//
//  Node.swift
//  Delegation
//
//  Created by Brian on 2/25/17.
//  Copyright © 2017 Unplugged Systems, LLC. All rights reserved.
//

import UIKit

protocol CustomNodeDelegate: class {
    func preferredNodeImage(node: Node) -> UIImage?
}

class Node: UIView {
    
    public var nodeDelegate: CustomNodeDelegate?
    var imageView: UIImageView? // optional value
    
    // MARK: Initializaton
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.gray
        imageView = UIImageView(image: UIImage(named: "circle.png"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Other Methods
    
    public func updateNode() {
         setPreferredNodeImage(node: self)
    }
    
    // MARK: Delegator calls to delegate
    
    func setPreferredNodeImage(node: Node) {
        
        if let tempImage = nodeDelegate?.preferredNodeImage(node: self) {
            self.imageView?.image = tempImage
            
            // check optional for a value and if value, unwrap
            if self.imageView != nil {
                addSubview(self.imageView!)
            }
        }
        else {
            print("Image error!")
        }
        
    }
    
}
