//
//  ViewController.swift
//  Delegation
//
//  Created by Brian on 2/24/17.
//  Copyright © 2017 Unplugged Systems, LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController, CustomNodeDelegate {
    
    var node: Node? = nil

    // MARK: Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create delegator
        node = Node(frame: CGRect(x: 20, y: 20, width: self.view.frame.width - 40, height: 300))
        self.view.addSubview(node!)
        
        // set this view controller to be the delegate of the Node instance
        node?.nodeDelegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        node?.updateNode()
    }
    
    // MARK: Delegate method implementations
    
    func preferredNodeImage(node: Node)  -> UIImage? {
        print("preferredNodeImage delegate method being called")
        return UIImage(named: "star.png")
    }

}

